//
//  ViewModel.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import Foundation
import RxSwift

class ViewModel {
    
    var searchText = ""
    
    var SportLeagues: [League] = [] {
        didSet {
            self.reloadList()
        }
    }
    
    var searchLeagues: [[SearchLeagueElement]] = [] {
        didSet {
            self.reloadList()
        }
    }
    
    var teams: [TeamElement] = [] {
        didSet {
            self.reloadList()
        }
    }
    
    var error: Error? {
        didSet { self.showAlertClosure?() }
    }
    var isLoading: Bool = false {
        didSet { self.updateLoadingStatus?() }
    }
        
    // Dispose bag
    private let disposeBag = DisposeBag()
        
    // MARK: - Closures for callback, since we are not using the ViewModel to the View.
    var showAlertClosure: (() -> Void)?
    var errorMessage = {(message: String) -> Void in }
    var updateLoadingStatus: (() -> Void)?
    var didFinishFetch: (() -> Void)?
    var reloadList = {() -> Void in }
    var reloadTeamList = {() -> Void in }
    var reloadMove = {() -> Void in }
    
    func fetchLeagues() {
        ApiClient.getLeague()
                .observe(on: MainScheduler.instance)
                .subscribe(onNext: { leagueList in
                    guard let league = leagueList.leagues else {return}
                    self.SportLeagues = league
                                    
                }, onError: { error in
                    switch error {
                    case ApiError.conflict:
                        print("Conflict error")
                    case ApiError.forbidden:
                        print("Forbidden error")
                    case ApiError.notFound:
                        print("Not found error")
                    default:
                        print("Unknown error:", error)
                    }
                })
                .disposed(by: disposeBag)
    }
    
    func fetchSearchLeagues(league: String) {
        ApiClient.getSearchLeague(league: league)
                .observe(on: MainScheduler.instance)
                .subscribe(onNext: { leagueList in
                    self.searchLeagues = leagueList
                                    
                }, onError: { error in
                    switch error {
                    case ApiError.conflict:
                        print("Conflict error")
                    case ApiError.forbidden:
                        print("Forbidden error")
                    case ApiError.notFound:
                        print("Not found error")
                    default:
                        print("Unknown error:", error)
                    }
                })
                .disposed(by: disposeBag)
    }
    
    func fetchTeams(league: String) {
        ApiClient.getAllTeamLeague(league: league)
                .observe(on: MainScheduler.instance)
                .subscribe(onNext: { teamList in
//                    print(teamList.te)
                    guard let teams = teamList.teams else {return}

                    self.teams = teams
                                                        
                }, onError: { error in
                    switch error {
                    case ApiError.conflict:
                        print("Conflict error")
                    case ApiError.forbidden:
                        print("Forbidden error")
                    case ApiError.notFound:
                        print("Not found error")
                    default:
                        print("Unknown error:", error)
                    }
                })
                .disposed(by: disposeBag)
    }
    
}
