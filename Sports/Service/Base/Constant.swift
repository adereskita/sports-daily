//
//  Constant.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import Foundation

struct Constants {
    
// The API's base URL
    
    static let URL = "https://www.thesportsdb.com/api/v1/json/2/"

    // parameters (Queries)
    struct Parameters {
        static let player = "p"
        static let id = "id"
        static let league = "l"
    }
    
// header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //use content type as (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}

