//
//  ApiRouter.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible {
    
// endpoint name
    case getLeague
    case getSports
    case getAllTeamLeague(league: String)
    case getSearchLeague(league: String)

    
// MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
// Http method
        urlRequest.httpMethod = method.rawValue
        
// Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        
// Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    // MARK: - HttpMethod
    // This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .getLeague:
            return .get
        case .getSports:
            return .get
        case .getAllTeamLeague:
            return .get
        case . getSearchLeague:
            return .get
        }
    }
    
    // MARK: - Path
    // the part following base url
    private var path: String {
        switch self {
        case .getLeague:
            return "all_leagues.php"
        case .getSports:
            return "all_sports.php"
        case .getAllTeamLeague:
            return "search_all_teams.php"
        case .getSearchLeague:
            return "search_all_leagues.php"
        }
    }
    
    // MARK: - Parameters
    // the queries, optional
    private var parameters: Parameters? {
        switch self {
        case .getSearchLeague(let league):
            return [Constants.Parameters.league: league]
        case .getAllTeamLeague(let league):
            return [Constants.Parameters.league: league]
        case .getLeague:
            return nil
        case .getSports:
            return nil
        }
    }
}


