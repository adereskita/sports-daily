//
//  ApiClient.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import Alamofire
import RxSwift

class ApiClient {
    
    static func getLeague() -> Observable<AllLeagueModel> {
        return request(ApiRouter.getLeague)
    }
    static func getSports() -> Observable<AllSportsModel> {
        return request(ApiRouter.getSports)
    }
    static func getAllTeamLeague(league: String) -> Observable<AllTeams> {
        return request(ApiRouter.getAllTeamLeague(league: league))
    }
    static func getSearchLeague(league: String) -> Observable<SearchLeague> {
        return request(ApiRouter.getSearchLeague(league: league))
    }
    
// MARK: - The request function to get results in an Observable
    private static func request<T: Codable> (_ urlConvertible: URLRequestConvertible) -> Observable<T> {
// an RxSwift observable, which will be the one to call the request when subscribed to
        return Observable<T>.create { observer in
// HttpRequest using AlamoFire (AF)
            let request = AF.request(urlConvertible).responseDecodable { (response: AFDataResponse<T>) in
                switch response.result {
                case .success(let value):
// return the value in onNext
                    observer.onNext(value)
                    observer.onCompleted()
                case .failure(let error):
                    switch response.response?.statusCode {
                    case 403:
                        observer.onError(ApiError.forbidden)
                    case 404:
                        observer.onError(ApiError.notFound)
                    case 409:
                        observer.onError(ApiError.conflict)
                    case 500:
                        observer.onError(ApiError.internalServerError)
                    default:
                        observer.onError(error)
                    }
                }
            }
            
 // return a disposable to stop the request
            return Disposables.create {
                request.cancel()
            }
        }
    }
}

