//
//  DetailVC.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import UIKit
import SDWebImage
import CoreData

class DetailVC: UIViewController {
    
    var id: String = ""
    var like: String = ""
    var strLeague: String = ""
    var strTeam: String = ""
    var strSport: String = ""
    var imageStadium: String = ""
    var imageJersey: String = ""
    var imageTeamLogo: String = ""
    var desc: String = ""
    var isFavorited = false
    
    var viewModel = ViewModel()
    var FavoriteVC = MyFavoriteTableVC()
    weak var favDelegate: MyFavoriteTableDelegate?
       
    let fav = UIImage(systemName: "star")
    let favFill = UIImage(systemName: "star.fill")
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lbLeague: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbType: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var btnSaveTeams: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var favTeamBtn: UIButton!
    @IBOutlet weak var bgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        favDelegate?.viewWillAppears()
    }
    
    func SetupUI() {
        self.imgDetail.clipsToBounds = false
                
        self.imgDetail.layer.shadowColor = #colorLiteral(red: 0.4274509804, green: 0.5529411765, blue: 0.6784313725, alpha: 1)
        self.imgDetail.layer.shadowOpacity = 0.35
        imgDetail.layer.shadowOffset = CGSize(width: 0.4, height: 1.4)
        imgDetail.layer.shadowRadius = 10
                
        imgDetail.layer.cornerRadius = 40
        imgDetail.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        self.bottomView.layer.cornerRadius = 20
        self.bottomView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        self.bottomView.layer.shadowColor = #colorLiteral(red: 0.4274509804, green: 0.5529411765, blue: 0.6784313725, alpha: 1)
        self.bottomView.layer.shadowOpacity = 0.25
        self.bottomView.layer.shadowRadius = 10
        
        favTeamBtn.layer.cornerRadius = 10
        self.favTeamBtn.layer.shadowColor = #colorLiteral(red: 0.4274509804, green: 0.5529411765, blue: 0.6784313725, alpha: 1)
        self.favTeamBtn.layer.shadowOpacity = 0.25
        self.favTeamBtn.layer.shadowRadius = 10
                
        self.lbName.text = self.strTeam
        self.lbType.text = self.strSport
        self.lbDescription.text = self.desc
        self.lbLeague.text = self.strLeague
        
        self.btnSaveTeams.layer.cornerRadius = 20

        self.stackView.layer.cornerRadius = 5
        self.backBtn.layer.cornerRadius = 10
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DetailVC.backTapped(gesture:)))
        backBtn.addGestureRecognizer(tapGesture)
        backBtn.isUserInteractionEnabled = true
        self.backBtn.tintColor = .white
        
        setupButtonState()
        
        guard let urlStadium = URL(string: self.imageStadium) else { return }
        self.imgDetail.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.imgDetail.sd_setImage(with: urlStadium, placeholderImage: UIImage(systemName: "photo"), completed: nil)
    }
    
    func setupButtonState() {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
            let predicate = NSPredicate(format: "teamID == \(self.id)")

            let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Sports")
            fetchRequest.predicate = predicate

             do {
                 guard let results = try? context.fetch(fetchRequest) else { return }

                 if results.isEmpty {
                     self.isFavorited = false
                     favTeamBtn.setImage(fav, for: .normal)
                 } else {
                     self.isFavorited = true
                     favTeamBtn.setImage(favFill, for: .normal)
                 }
             }
    }
    
    func FavoriteTeam(isbtn: Bool) {
        if self.isFavorited == true {
            favTeamBtn.setImage(fav, for: .normal)
        } else {
            favTeamBtn.setImage(favFill, for: .normal)
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let predicate = NSPredicate(format: "teamID == \(self.id)")
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Sports")
        fetchRequest.predicate = predicate
        
        do {
            guard let results = try? context.fetch(fetchRequest) else { return }
            
            if results.isEmpty {
                let entity = NSEntityDescription.entity(forEntityName: "Sports", in: context)
                let newFavorite = Sports(entity: entity!, insertInto: context)
                newFavorite.id = Int32(self.id)!
                newFavorite.teamID = self.id
                newFavorite.strLeague = self.strLeague
                newFavorite.strTeam = self.strTeam
                newFavorite.strSport = self.strSport
                newFavorite.imageStadium = self.imageStadium
                newFavorite.imageTeamLogo = self.imageTeamLogo
                newFavorite.imageJersey = self.imageJersey
                newFavorite.desc = self.desc
                newFavorite.like = self.like

                        
                do {
                    print("save complete.")
                    try context.save()
                    FavoriteVC.myTeamList.append(newFavorite)
                    favTeamBtn.setImage(favFill, for: .normal)
                    showAlertSucceed()
                } catch {
                    print("context save error.")
                }
                
            } else {
                if isbtn == true {
                    favTeamBtn.setImage(favFill, for: .normal)
                    showAlertSucceed()
                } else {
                    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

                    if let result = try? context.fetch(fetchRequest) {
                        for object in result {
            //                        context.delete(object as! NSManagedObject)
                            try context.execute(batchDeleteRequest)
                            print("delete complete.")
                            favTeamBtn.setImage(fav, for: .normal)
                            showAlertDelete()
                        }
                    }
                }
            }
        } catch {
            print("Fetch Failed..")
        }
    }
    
    func showAlertSucceed() {
        let alert = UIAlertController(title: "Team is On Your List.", message: "Team will show in Favorite list.", preferredStyle: .alert)
            
        alert.addAction(UIAlertAction(title: "Discard", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertDelete() {
        let alert = UIAlertController(title: "Team Removed.", message: "Team is Removed from your list.", preferredStyle: .alert)
            
        alert.addAction(UIAlertAction(title: "Discard", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func backTapped(gesture: UIGestureRecognizer) {
        favDelegate?.viewWillAppears()
        self.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func Favorite(_ sender: Any) {
        FavoriteTeam(isbtn: false)
    }
        
    @IBAction func saveTeam(_ sender: Any) {
        FavoriteTeam(isbtn: true)

    }
    
}
