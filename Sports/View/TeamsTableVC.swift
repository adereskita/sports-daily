//
//  TeamsTableVC.swift
//  Sports
//
//  Created by Ade Reskita on 17/02/22.
//

import UIKit

class TeamsTableVC: UITableViewController {
    
    var id: String = ""
    var strLeague: String = ""
    var strSport: String = ""
    
    var viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        SetupUI()
        viewModel.fetchTeams(league: self.strLeague)
        BindViewModel()
    }
    
    func SetupUI() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func BindViewModel() {
        viewModel.reloadList = { [weak self] ()  in
            // UI chnages in main tread
            DispatchQueue.main.async {
                self!.tableView.reloadData()
            }
        }

        viewModel.showAlertClosure = {
            if let error = self.viewModel.error {
                print(error.localizedDescription)
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.teams.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell", for: indexPath) as! TeamsTableViewCell
        let teamsObj = viewModel.teams[indexPath.row]

        cell.setTeamsData(withViewModel: teamsObj)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
        
        let teamObj = viewModel.teams[indexPath.row]
                
        vc.id = teamObj.idTeam!
        vc.imageTeamLogo = teamObj.strTeamBadge!
//        vc.imageJersey = teamObj.strTeamJersey!
        vc.desc = teamObj.strDescriptionEN!
        vc.strTeam = teamObj.strTeam!.capitalized
        vc.strLeague = teamObj.strLeague!.capitalized
        vc.strSport = teamObj.strSport!.capitalized
        
        if let stadium = teamObj.strStadiumThumb {
            vc.imageStadium = stadium
        } else {
            vc.like = ""
        }
        
        if let loved = teamObj.intLoved {
            vc.like = loved
        } else {
            vc.like = "0"
        }
                                    
        tableView.deselectRow(at: indexPath, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
}
