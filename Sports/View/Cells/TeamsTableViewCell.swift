//
//  TeamsTableViewCell.swift
//  Sports
//
//  Created by Ade Reskita on 17/02/22.
//

import UIKit
import SDWebImage

class TeamsTableViewCell: UITableViewCell {
    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var teamNameLbl: UILabel!
    @IBOutlet weak var teamTypeLbl: UILabel!
    @IBOutlet weak var teamLikedLbl: UILabel!
    
    static let identifier = "TeamsTableViewCell"

    static func nib() -> UINib {
        return UINib(nibName: "TeamsTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setTeamsData(withViewModel viewModel: TeamElement) {
        var imageUrlString: String?
        imageUrlString = viewModel.strTeamBadge!
        
        teamNameLbl.text = viewModel.strTeam
        teamTypeLbl.text = viewModel.strLeague
        if let loved = viewModel.intLoved {
            teamLikedLbl.text = loved
        } else {
            teamLikedLbl.text = "0"
        }
        
        guard let imageURL = URL(string: imageUrlString!) else {return}
        self.teamImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.teamImageView.sd_setImage(with: imageURL)
    }

}
