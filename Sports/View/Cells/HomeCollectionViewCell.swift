//
//  HomeCollectionViewCell.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "HomeCollectionViewCell"

    static func nib() -> UINib {
        return UINib(nibName: "HomeCollectionViewCell", bundle: nil)
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cellActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbGenre: UILabel!
    @IBOutlet weak var cardView: UIView!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
            
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        imageView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            
        self.cellActivityIndicator.hidesWhenStopped = true
            
        cardView.layer.cornerRadius = 10
        cardView.layer.shadowColor = #colorLiteral(red: 0.4274509804, green: 0.5529411765, blue: 0.6784313725, alpha: 1)
        cardView.layer.shadowOpacity = 0.25
        cardView.layer.shadowOffset = CGSize(width: 0.4, height: 1.4)
        cardView.layer.shadowRadius = 10
        cardView.layer.shouldRasterize = true
    }
    
    func setImageSearch(withViewModel viewModel: SearchLeagueElement) {
        self.lbGenre.text = viewModel.strSport
        self.lbTitle.text = viewModel.strLeague
    }
    
    func setImage(withViewModel viewModel: League) {
        
        self.lbGenre.text = viewModel.strSport
        self.lbTitle.text = viewModel.strLeague
        
    }

}
