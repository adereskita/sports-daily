//
//  MyFavoriteTableViewCell.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import UIKit
import SDWebImage

class MyFavoriteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var teamImageView: UIImageView!
    @IBOutlet weak var teamNameLbl: UILabel!
    @IBOutlet weak var teamTypeLbl: UILabel!
    @IBOutlet weak var teamLikedLbl: UILabel!
    
    static let identifier = "MyFavoriteTableViewCell"

    static func nib() -> UINib {
        return UINib(nibName: "MyFavoriteTableViewCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setTeamsData(withCoreData sports: Sports) {
        var imageUrlString: String?
        imageUrlString = sports.imageTeamLogo!
        
        teamNameLbl.text = sports.strTeam
        teamTypeLbl.text = sports.strLeague
        guard let loved = sports.like else {return}
        teamLikedLbl.text = loved

        guard let imageURL = URL(string: imageUrlString!) else { return }
        self.teamImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.teamImageView.sd_setImage(with: imageURL)
    }

}
