//
//  MyFavoriteTableVC.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import UIKit
import CoreData

protocol MyFavoriteTableDelegate: AnyObject {
    func viewWillAppears()
}

class MyFavoriteTableVC: UITableViewController {
    
    var myTeamList = [Sports]()
    var initialLaod = true
    
    @IBOutlet weak var myTeamListTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func fetchData() {
        if initialLaod {
            initialLaod = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Sports")
                   
            do {
                let results: NSArray = try context.fetch(request) as NSArray
                print(results)
                   
                for result in results {
                    let favTeam = result as! Sports
                    myTeamList.append(favTeam)
                    tableView.reloadData()
                }
            } catch {
                print("Fetch Failed..")
            }
        } else {
            myTeamList.removeAll()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Sports")
                
            do {
                let results: NSArray = try context.fetch(request) as NSArray
                    
                for result in results {
                    let favsTeam = result as! Sports
                    self.myTeamList.append(favsTeam)
                    self.tableView.reloadData()
                }
            } catch {
                print("Fetch Failed..")
            }
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myTeamList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTeamListTableView.dequeueReusableCell(withIdentifier: MyFavoriteTableViewCell.identifier, for: indexPath) as! MyFavoriteTableViewCell
                
        let teamObj = myTeamList[indexPath.row]
                
        cell.setTeamsData(withCoreData: teamObj)
        return cell
    }
            
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailVC
                
        let teamObj = myTeamList[indexPath.row]
                
        vc.id = teamObj.teamID!
        vc.like = teamObj.strLeague!
        vc.imageTeamLogo = teamObj.imageTeamLogo!
        vc.imageJersey = teamObj.imageJersey!
        vc.imageStadium = teamObj.imageStadium!
        vc.desc = teamObj.desc!
        vc.strTeam = teamObj.strTeam!.capitalized
        vc.strLeague = teamObj.strLeague!.capitalized
        vc.strSport = teamObj.strSport!.capitalized
                                
        tableView.deselectRow(at: indexPath, animated: true)
        vc.favDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let teamObj = myTeamList[indexPath.row]

        if editingStyle == .delete {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
            let predicate = NSPredicate(format: "teamID == \(teamObj.teamID!)")
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Sports")
            fetchRequest.predicate = predicate
            
            do {
                guard let results = try? context.fetch(fetchRequest) else { return }
                
                if !results.isEmpty {
                    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

                    if let result = try? context.fetch(fetchRequest) {
                        for object in result {
                            try context.execute(batchDeleteRequest)
                            self.myTeamList.remove(at: indexPath.row)
                            self.tableView.deleteRows(at: [indexPath], with: .automatic)
                            print("delete complete.")
                        }
                    }
                }
            } catch {
                print("Fetch Failed..")
            }
        }
    }
}

extension MyFavoriteTableVC: MyFavoriteTableDelegate {
    func viewWillAppears() {
        myTeamList.removeAll()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Sports")
            
        do {
            let results: NSArray = try context.fetch(request) as NSArray
                
            for result in results {
                let favsTeam = result as! Sports
                self.myTeamList.append(favsTeam)
                self.tableView.reloadData()
            }
        } catch {
            print("Fetch Failed..")
        }
    }
}
