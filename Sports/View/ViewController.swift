//
//  ViewController.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collViewHome: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var viewModel = ViewModel()
    var loadingView: HomeCollectionReusableView?
    var isLoading = false
    var isSearching = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SetupUI()
        viewModel.fetchLeagues()
        BindViewModel()
    }
    
    func SetupUI() {
        self.collViewHome.register(HomeCollectionViewCell.nib(), forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
        // MARK: - Loading View
        let loadingReusableView = UINib(nibName: "HomeCollectionReusableView", bundle: nil)
        self.collViewHome.register(loadingReusableView, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: HomeCollectionReusableView.identifier)

        // MARK: - Keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

        let layout = collViewHome.collectionViewLayout as! UICollectionViewFlowLayout
        layout.estimatedItemSize = .zero
        
        collViewHome.delegate = self
        collViewHome.dataSource = self
        searchBar.delegate = self
    }
    
    func BindViewModel() {
        viewModel.reloadList = { [weak self] ()  in
            // UI chnages in main tread
            DispatchQueue.main.async {
                self?.collViewHome.reloadData()
            }
        }

        viewModel.showAlertClosure = {
            if let error = self.viewModel.error {
                print(error.localizedDescription)
            }
        }
    }
    
    func loadMoreData() {
        if !self.isLoading && self.isSearching == false {
            self.isLoading = true
                        
            DispatchQueue.global().async {
                // Fake background loading task 1 seconds
                sleep(1)
                // Download more data
                self.viewModel.fetchLeagues()
                self.BindViewModel()
                        
                DispatchQueue.main.async {
                    self.isLoading = false
                }
            }
        } else {
            self.isLoading = true
                        
            DispatchQueue.global().async {
                sleep(1)
                self.viewModel.fetchLeagues()
                self.BindViewModel()
                        
                DispatchQueue.main.async {
                    self.isLoading = false
                }
            }
        }
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
//        isSearching = false
    }
}


extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching == true {
            return viewModel.searchLeagues.count
        } else {
            return viewModel.SportLeagues.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isSearching == true {
            let cell = collViewHome.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
            let leagueSearchObj = viewModel.searchLeagues[indexPath.row]
            
            cell.setImageSearch(withViewModel: leagueSearchObj[indexPath.row])
            return cell
        } else {
            let cell = collViewHome.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
            let sportLeagueObj = viewModel.SportLeagues[indexPath.row]
            
            cell.setImage(withViewModel: sportLeagueObj)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TeamsTableVC") as! TeamsTableVC
        
        if isSearching == true {
            let leagueObj = viewModel.searchLeagues[indexPath.row][indexPath.row]
                    
            vc.id = leagueObj.idLeague!
            vc.strLeague = leagueObj.strLeague!.capitalized
            vc.strSport = leagueObj.strSport!.capitalized
            
        } else {
            let leagueObj = viewModel.SportLeagues[indexPath.row]
                    
            vc.id = leagueObj.idLeague!
            vc.strLeague = leagueObj.strLeague!.capitalized
            vc.strSport = leagueObj.strSport!.capitalized
        }
                                
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if let navCtrl = self.navigationController {
           navCtrl.pushViewController(vc, animated: true)
        } else {
           let navCtrl = UINavigationController(rootViewController: vc)
            self.present(navCtrl, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isSearching == true {
            let lastColl = viewModel.SportLeagues.count - 1
            if indexPath.row == lastColl && !self.isLoading {
//                self.viewModel.SportLeagues.count += 10
                    loadMoreData()
                }
            } else {
                let lastColl = viewModel.SportLeagues.count - 1
                if indexPath.row == lastColl && !self.isLoading {
//                    self.viewModel.limit += 10
                    loadMoreData()
                }
            }
        }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
            
    // MARK: - Mengatur Layout Loading Cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.isLoading {
            return CGSize.zero
        } else {
            return CGSize(width: collectionView.bounds.size.width, height: 52)
        }
    }
            
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HomeCollectionReusableView.identifier, for: indexPath) as! HomeCollectionReusableView
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
            return aFooterView
        }
        return UICollectionReusableView()
    }
            
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.startAnimating()
        }
    }
            
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.stopAnimating()
        }
    }
    
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            isSearching = false
            viewModel.SportLeagues = []
            viewModel.searchLeagues = []
//            viewModel.searchText = ""
            viewModel.fetchLeagues()
        } else {
            isSearching = true
        }
    }
            
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.collViewHome.reloadData()
        view.endEditing(true)
    }
            
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.SportLeagues = []
        viewModel.searchLeagues = []
        viewModel.searchText = searchBar.text!
        viewModel.fetchSearchLeagues(league: searchBar.text!)
        BindViewModel()
        self.collViewHome.reloadData()
        view.endEditing(true)
    }
}
