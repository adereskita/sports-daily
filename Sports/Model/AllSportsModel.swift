//
//  AllSportsModel.swift
//  Sports
//
//  Created by Ade Reskita on 16/02/22.
//

import Foundation

// MARK: - AllSportModel
struct AllSportsModel: Codable {
    var sports: [Sport]?
}

// MARK: - Sport
struct Sport: Codable {
    var idSport, strSport: String?
    var strFormat: StrFormat?
    var strSportThumb: String?
    var strSportIconGreen: String?
    var strSportDescription: String?
}

enum StrFormat: String, Codable {
    case eventSport = "EventSport"
    case teamvsTeam = "TeamvsTeam"
}


// MARK: - AllLeagueModel
struct AllLeagueModel: Codable {
    var leagues: [League]?
}

// MARK: - League
struct League: Codable {
    var idLeague, strLeague, strSport: String?
    var strLeagueAlternate: String?
}


// MARK: - SearchLeagueElement
struct SearchLeagueElement: Codable {
    var idLeague: String?
    var idSoccerXML: JSONNull?
    var idAPIfootball, strSport, strLeague, strLeagueAlternate: String?
    var intDivision, idCup, strCurrentSeason, intFormedYear: String?
    var dateFirstEvent, strGender, strCountry, strWebsite: String?
    var strFacebook: String?
    var strInstagram: JSONNull?
    var strTwitter, strYoutube, strRSS, strDescriptionEN: String?
    var strDescriptionDE, strDescriptionFR, strDescriptionIT, strDescriptionCN: String?
    var strDescriptionJP, strDescriptionRU, strDescriptionES, strDescriptionPT: String?
    var strDescriptionSE, strDescriptionNL, strDescriptionHU, strDescriptionNO: String?
    var strDescriptionPL, strDescriptionIL, strTvRights: String?
    var strFanart1, strFanart2, strFanart3, strFanart4: String?
    var strBanner: String?
    var strBadge, strLogo: String?
    var strPoster: String?
    var strTrophy: String?
    var strNaming, strComplete, strLocked: String?
}

typealias SearchLeague = [[SearchLeagueElement]]

// MARK: - AllTeams
struct AllTeams: Codable {
    var teams: [TeamElement]?
}

// MARK: - Sport
struct TeamElement: Codable {
    var idTeam, intLoved, strTeam, strLeague, strSport: String?
    var strTeamBadge, strTeamJersey, strTeamLogo: String?
    var strStadiumThumb, strStadium: String?
    var strFormat: StrFormat?
    var strSportThumb: String?
    var strSportIconGreen: String?
    var strSportDescription, strStadiumDescription, strStadiumLocation, intStadiumCapacity: String?
    var strDescriptionEN: String?
}

typealias ListTeams = [[TeamElement]]

//struct AllTeams: Codable {
//    var teams: [[String: String?]]?
//}





// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
