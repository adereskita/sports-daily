//
//  extSports.swift
//  Sports
//
//  Created by Ade Reskita on 17/02/22.
//

import Foundation
import CoreData

extension Sports {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Sports> {
        return NSFetchRequest<Sports>(entityName: "Sports")
    }

//    @NSManaged public var id: NSManagedObjectID
    @NSManaged public var id: Int32
    @NSManaged public var teamID: String?
    @NSManaged public var strTeam: String?
    @NSManaged public var strSport: String?
    @NSManaged public var strLeague: String?
    @NSManaged public var like: String?
    @NSManaged public var imageTeamLogo: String?
    @NSManaged public var imageStadium: String?
    @NSManaged public var imageJersey: String?
    @NSManaged public var desc: String?
}

extension Sports: Identifiable {

}
